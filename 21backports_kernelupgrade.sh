#!/bin/sh

set -e

if [[ "$(whoami)" != root ]]; then
	exec sudo -- "$0" "$@"
fi

SOURCE_LIST="/etc/apt/sources.list"

chattr -i $SOURCE_LIST

cp -v $SOURCE_LIST $SOURCE_LIST.debsave

cat > $SOURCE_LIST.new <<EOF
# Pardus Yirmibir
#deb http://depo.pardus.org.tr/pardus yirmibir main contrib non-free
#deb-src http://depo.pardus.org.tr/pardus yirmibir main contrib non-free

#deb http://depo.pardus.org.tr/guvenlik yirmibir main contrib non-free
#deb-src http://depo.pardus.org.tr/guvenlik yirmibir main contrib non-free

# Pardus Yirmibir Backports
deb http://depo.pardus.org.tr/backports/ yirmibir-backports main contrib non-free
#deb-src http://depo.pardus.org.tr/backports yirmibir-backports main contrib non-free
EOF

mv -fv $SOURCE_LIST.new $SOURCE_LIST

DEBIAN_FRONTEND=noninteractive apt update
DEBIAN_FRONTEND=noninteractive apt install -yy -o Dpkg::Options::="--force-confnew" -t yirmibir-backports linux-image-amd64

exit 0
