#!/usr/bin/env bash


read -p "Toplu isim değişikliği yapacağınız uzantıyı belirtin (örn: pdf, jpg, png): " uzanti

read -p "Dosya ismi hangi rakamdan başlasın: " basla

adet=$(find . -depth -iname "*.$uzanti" -type f | wc -l)

bitir=$((adet+basla-1))

# Dosyaa adında boşluk olanları bul
find . -depth -iname "*.$uzanti" | while read eski_pdf_dosyasi
do
	mv "$eski_pdf_dosyasi" "$basla".$uzanti
   ((basla++))
done
