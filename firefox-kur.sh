#!/bin/bash

if [[ "$(whoami)" != root ]]; then
	exec sudo -- "$0" "$@"
fi

USER_HOME=$(dirname $XAUTHORITY)

##Son Sürüm Firefoxu İndiriyoruz
wget -c -O /tmp/firefox.tar.bz2 "https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=tr"  --no-check-certificate

##Arşivi /opt dizinie çıkarıyoruz
sudo tar -xvjf /tmp/firefox.tar.bz2 -C /opt

##Başlatıcı Yolu Eğer Oluşmamışsa Oluşturalım
mkdir -p $USER_HOME/.local/share/applications/

##Başlatıcıyı Oluşturalım
touch $USER_HOME/.local/share/applications/firefox.desktop

##Başlatıcı İçeriğini Yazalım
echo "[Desktop Entry]
Categories=Network;WebBrowser;
Comment=Surf on the web
Comment[tr]=Web'de gezin
Exec=/opt/firefox/firefox %u
GenericName=Web Browser
GenericName[tr]=Web Tarayıcı
Icon=firefox-esr
MimeType=text/html;image/png;image/jpeg;image/gif;application/xml;application/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;
Name[tr_TR]=Firefox
Name=Firefox
Path=
StartupNotify=true
StartupWMClass=Firefox
Terminal=false
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=
X-GNOME-FullName=Firefox Web Browser
X-GNOME-FullName[tr]=Firefox Web Tarayıcı
X-KDE-RunOnDiscreteGpu=false
X-KDE-SubstituteUID=false
X-KDE-Username=
X-MultipleArgs=false" > $USER_HOME/.local/share/applications/firefox.desktop

##Komutların Tamamlandı
echo "Komutlar Tamamlandı Uçbirim Çıktısını Kontrol Edin"
ln -sfr /opt/firefox/firefox /usr/bin/firefox
ln -sfr /opt/firefox/firefox /usr/bin/firefox-esr
