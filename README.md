<details>
<summary>siyah-ekran.sh dosyası</summary>

# siyah-ekran.sh dosyası
- Bu kodu uçbirime yapıştırıp çalıştırısanız, herşeyi kendisi otomatik yapacaktır.
- `curl -sL https://bit.ly/3rGhpBh -k | sudo bash -`
</details>

<details>
<summary>Gitlab depo oluşturma vs. örnekleri</summary>

# Git global setup
* `git config --global user.name "Yahya YILDIRIM"`
* `git config --global user.email "yahya.yldrm@gmail.com"`


# Yeni bir depo oluştur
* `git clone git@gitlab.com:yahyayildirim/test.git`
* `cd test`
* `git switch -c main`
* `touch README.md`
* `git add README.md`
* `git commit -m "add README"`
* `git push -u origin main`


# Push an existing folder
* `cd existing_folder`
* `git init --initial-branch=main`
* `git remote add origin git@gitlab.com:yahyayildirim/test.git`
* `git add .`
* `git commit -m "Initial commit"`
* `git push -u origin main`


# Push an existing Git repository
* `cd existing_repo`
* `git remote rename origin old-origin`
* `git remote add origin git@gitlab.com:yahyayildirim/test.git`
* `git push -u origin --all`
* `git push -u origin --tags`
</details>

<details>
<summary>Aşağıdaki işlemleri yapabilmeniz için PARDUS İSO yüklü olan USB/FLASH BELLEK ile sistemi BOOT edip CANLI/LİVE olarak açmanız gerekmektedir.</summary>

# EFI BOOT
<details>
<summary>SYSTEMD-BOOT</summary>

- 2014'ten sonra satılan çoğu bilgisayar UEFI modunu kullanır. `sudo parted -ls` komutunu uçbirimde çalıştırdığınızda, eğer flags başlığı altında `boot,esp` var ise, sisteminiz **UEFI** modunda kurulmuş, `bios_grub` var ise sisteminiz **LEGACY** olarak kurulmuştur.

- Ayrıca uç birimden `[ -d /sys/firmware/efi ] && echo "Installed in UEFI mode" || echo "Installed in Legacy mode"` komutu ile de işletim sisteminin UEFI modunda kurulu olup olmadığını görebilirsiniz.


## Eğer diskiniz NVMe ise;
- `sudo mount /dev/nvme0n1p3 /mnt`
- `sudo mount /dev/nvme0n1p1 /mnt/boot/efi`

## Eğer diskiniz SATA ise;
- `sudo mount /dev/sda3 /mnt`
- `sudo mount /dev/sda1 /mnt/boot/efi`

## Ardından, her iki disk türü için de aşağıdaki komutlarla devam edin:
- `for i in dev dev/pts proc sys run; do sudo mount -B /$i /mnt/$i; done`
- `sudo chroot /mnt`
- `apt install --reinstall linux-image-amd64 linux-headers-amd64`
- `update-initramfs -c -k all`
- `exit`
- `sudo bootctl --path=/mnt/boot/efi install`
</details>


# GRUB BOOT
<details>
<summary>EFI BOOT</summary>

- 2014'ten sonra satılan çoğu bilgisayar UEFI modunu kullanır. `sudo parted -ls` komutunu uçbirimde çalıştırdığınızda, eğer flags başlığı altında `boot,esp` var ise, sisteminiz **UEFI** modunda kurulmuş demektir.

- Ayrıca uç birimden `[ -d /sys/firmware/efi ] && echo "Installed in UEFI mode" || echo "Installed in Legacy mode"` komutu ile de işletim sisteminin hangi modda kurulu olduğunu görebilirsiniz.


## Eğer diskiniz NVMe ise;
- `sudo mount /dev/nvme0n1p2 /mnt`
- `sudo mount /dev/nvme0n1p1 /mnt/boot/efi`

## Eğer diskiniz SATA ise;
- `sudo mount /dev/sda2 /mnt`
- `sudo mount /dev/sda1 /mnt/boot/efi`

## Ardından, her iki disk türü için de aşağıdaki komutlarla devam edin:
- `for i in dev dev/pts proc sys run; do sudo mount -B /$i /mnt/$i; done`
- `sudo chroot /mnt`
- `apt install --reinstall grub-efi-amd64 linux-image-amd64 linux-headers-amd64`
- `update-initramfs -c -k all`
- `update-grub`
</details>

<details>
<summary>LEGACY BİOS BOOT</summary>

- `sudo parted -ls` komutunu uçbirimde çalıştırdığınızda, eğer flags başlığı altında `bios_grub` var ise sisteminiz **LEGACY** olarak kurulmuş demektir.

- Ayrıca uç birimden `[ -d /sys/firmware/efi ] && echo "Installed in UEFI mode" || echo "Installed in Legacy mode"` komutu ile de işletim sisteminin hangi modda kurulu olduğunu görebilirsiniz.


## Eğer diskiniz NVMe ise;
- `sudo mount /dev/nvme0n1p3 /mnt`

## Eğer diskiniz SATA ise;
- `sudo mount /dev/sda3 /mnt`

## Ardından, her iki disk türü için de aşağıdaki komutlarla devam edin:
- `for i in dev dev/pts proc sys run; do sudo mount -B /$i /mnt/$i; done`
- `sudo chroot /mnt`
- `apt install --reinstall grub-efi-amd64 linux-image-amd64 linux-headers-amd64`
- `update-initramfs -c -k all`
- `sudo update-grub`
</details>

# CHROOT
<details>
<summary>CHROOT</summary>

- chroot, mevcut işletim sistemi önyüklenmiş gibi komut çalıştırmanın bir yoludur. Chroot komutları çalıştırıldıktan sonra paket yöneticisi (apt) ve diğer sistem düzeyindeki komutlar çalıştırılabilir.

 EFI bölümü genellikle 512 MB civarındadır ve bu, bir sonraki komutun yerine geçecek bölümdür.  Kurtarma bölümü yaklaşık 4 GB'dir

## Eğer diskiniz NVMe ise;
- `sudo mount /dev/nvme0n1p1 /mnt/boot/efi`

## Eğer diskiniz SATA ise;
- `sudo mount /dev/sda1 /mnt/boot/efi`

## Ardından, her iki disk türü için de aşağıdaki komutlarla devam edin:
- `for i in dev dev/pts proc sys run; do sudo mount -B /$i /mnt/$i; done`
- `sudo chroot /mnt`

</details>
</details>
