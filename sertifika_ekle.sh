#!/usr/bin/env bash

YESIL="\033[0;32m"
KIRMIZI="\033[0;31m"
RENK_YOK="\033[0m"


function kullanim {
  echo -e -n "${KIRMIZI}"
  echo -e "Hata: Sertifika dosyasının yolunu belirtmeniz gerekmektedir..."
  echo -e -n "${RENK_YOK}"
  echo -e "\nÖrnek Kullanım:\n${YESIL}sudo bash sertifika_ekle.sh /home/$SUDO_USER/diyanet.crt\n"
  echo -e -n "${RENK_YOK}"
  exit 1

}

if [ -z "$(which certutil)" ]; then
    echo -e -n "${YESIL}"
    apt install -f libnss3-tool -yy
    echo -e -n "${RENK_YOK}"
fi

if [ -z "$1" ]
  then
    kullanim
fi

sertifika_dosyasi="$1"
sertifika_adi="paloalto.dib.local"

for cert9 in $(find /home/$SUDO_USER/.mozilla/ -name "cert9.db")
do
  cert_dir=$(dirname ${cert9});

  echo -e -n "${YESIL}Mozilla Firefox için Diyanet İşleri Başkanlığı sertfikası ('${sertifika_adi}'), ${cert_dir} klasörüne eklenmiştir.${RENK_YOK}\n"
  certutil -A -n "${sertifika_adi}" -t "TCu,Cuw,Tuw" -i ${sertifika_dosyasi} -d sql:"${cert_dir}" 2>/dev/null

done

for cert9 in $(find /home/$SUDO_USER/.pki/nssdb/ -name "cert9.db")
do
  cert_dir=$(dirname ${cert9});
  echo -e -n "${YESIL}Google Chrome için Diyanet İşleri Başkanlığı sertfikası ('${sertifika_adi}'), ${cert_dir} klasörüne eklenmiştir.${RENK_YOK}\n"
  certutil -A -n "${sertifika_adi}" -t "TCu,Cuw,Tuw" -i ${sertifika_dosyasi} -d sql:"${cert_dir}" 2>/dev/null

done

#firefox için silme : certutil -d sql:$HOME/.mozilla/ -D -n paloalto.dib.local
#chrome için silme: certutil -d sql:$HOME/.pki/nssdb/ -D -n paloalto.dib.local