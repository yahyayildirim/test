#!/usr/bin/env bash
if [[ "$(whoami)" != root ]]; then
   exec sudo -- "$0" "$@"
fi

# if [[ "$(whoami)" != root ]]; then
#    echo -e "===>\tLütfen yetkili hesap ile işlem yapınız."
#    echo -e "===>\tÖrnek: sudo bash $0"
#    exit 1
# fi

#kullanici_home=$(dirname $XAUTHORITY)
#kullanici_name=$(echo $kullanici_home | sed 's|.*/||')
#ls -d */ | tr " " "\n" | cut -d "/" -f 1
# klas(){
#    # Klasör adında boşluk olanları bul ve alttire işareti ile değiştir.
#    find "$paylasim_dizini" -depth -iname '* *' -type d | while read eski_klasor
#    do
#       # Boşlukları "_" işareti ile değiştir.
#       local yeni_klasor=`echo $eski_klasor | sed -e 's/ /_/g'`
#       # Eğer aynı dosyadan varsa uyarı yazdır.
#       if [ -d "$yeni_klasor" ]; then
#          echo -e "$yeni_klasor klasörü zaten var."
#          continue
#       else
#          # Eğer aynı isimli dosya yoksa adını değiştir ve bilgi ver.
#          mv "$eski_klasor" "$yeni_klasor" 2>/dev/null
#       fi
#    done
# }

uyari=$(zenity --info --text="Paylaşım yapacağınız klasörde boşluk olmamasına dikkat ediniz, aksi halde klasöre ulaşamazsızız.")

paylasim_dizini=$(zenity --title "Lütfen Paylaşacağınız Klasörü Seçin" --file-selection --directory)


if [[ $paylasim_dizini = *" "* ]]; then

   yeni_dizin=$(echo $paylasim_dizini | sed -e 's|.*/||' -e 's/ /_/g')

   mv -f "${paylasim_dizini}" "${yeni_dizin}" 2>/dev/null
else
   yeni_dizin=$paylasim_dizini
fi


paylasim_adi=$(echo $paylasim_dizini | sed -e 's|.*/||' -e 's/ /_/g' -e 's/İ/i/g' | tr '[A-Z]' '[a-z]')


# paylasim_adi=$(echo $paylasim_dizini | sed 's|.*/||')
if [ ! -z "$paylasim_adi" ]; then
   echo "
[${paylasim_adi}]
   comment = Dosya Paylaşım Klasörü
   path = $yeni_dizin
   browseable = yes
   guest ok = yes
   read only = no
   public = yes
   writable = yes
   create mask = 0777
   directory mask = 0777
" >> /etc/samba/smb.conf
   zenity --info --text="Paylaşım başarılı bir şekilde oluşturuldu."
   systemctl restart smbd nmbd
else
   zenity --error --text="Paylaşım oluşturulamadı?"
fi