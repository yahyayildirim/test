#!/usr/bin/env bash

# 
# Bu scriptin amacı dosya adlarında boşluk var ise "_" işareti ile değiştirir.
# PDF dosyası olup .pdf uzantısı yazılı olmayanlara .pdf uzantısı ekler. 

# Dikkat edilmesi gereken husus ise PDF dosyalarının scriptin çalıştırdığı klasörde
# veya alt klasörlerde olmasıdır.



# Dosyaa adında boşluk olanları bul
find ./PDF/ -name '* *' | while read eski_pdf_dosyasi 
do
	# Boşlukları "_" işareti ile değiştir.
   yeni_pdf_dosyasi=`echo $eski_pdf_dosyasi | tr " " "_"`
   # Eğer aynı dosyadan varsa uyarı yazdır.
   if [ -e "$yeni_pdf_dosyasi" ]; then
      echo -e "$yeni_pdf_dosyasi dosyası zaten var."
   else
   	# Eğer aynı isimli dosya yoksa adını değiştir ve bilgi ver.
      mv "$eski_pdf_dosyasi" "$yeni_pdf_dosyasi" 2>/dev/null
   fi
done


# pdf uzantılı olmayan dosyaları bulun
for pdf in $(find ./PDF/ -depth -type f -not -name "*?pdf" -print)
do
	# file komutu ile bak, eğer application/pdf ise uzantısını .pdf olarak değiştir
	# yoksa herhangi bir şey yapma ve bilgi yazdır.
	if [ $(file --mime-type -b "$pdf") == "application/pdf" ]; then
		mv "$pdf" "$pdf".pdf
		echo -e "Dosyanın ismi $pdf.pdf olarak değiştirildi."
	fi
done
