#!/bin/bash
#################################################
# Yazan		: Yahya YILDIRIM 
# E-Posta	: yahya.yldrm@gmail.com	
# Web		: https://github.com/yahyayildirim/
# Bu script kişisel kullanım için oluşturulmuştur.
#################################################

kirmizi=$'\e[1;31m';
sifirla=$'\e[0m';
kalin=$'\e[1m';
baslik="${kirmizi}\e[4m###  SHOTCUT VİDEO DÜZENLEME PROGRAMI KURULUM SCRİPTİNE HOŞ GELDİNİZ  ###${sifirla}\n"
clear

echo -e $baslik

#İşletim Sistemi Mimarisi Tespit Etme
if [ "$(uname -m)" == "x86_64" ]; then
	ARCH_TYPE="x86_64"
else
	ARCH_TYPE="i386"
	echo "İşlemcinizin mimarisi 64 bit olmadığı için program kapatılmıştır..."
	exit
fi


# Shotcut Video Düzenleme Programı İndirme
PR_ADI=Shotcut
if [[ -e /usr/local/bin/shotcut ]]; then
	CUR_VER="$(shotcut -v | cut -d " " -f 2)"
fi

NEW_VER="$(curl -s https://www.shotcut.org/download/ | grep "Current Version" | awk -F '"' '{ print $2 }' | awk -F '-' '{ print $3 }')"
GIT_VER=$(sed -e "s/\(..\)\(..\)\(..\)/\1.\2.\3/" <<< "$NEW_VER")
PR_EK=txz
DS_ADI=${PR_ADI,,}-linux-${ARCH_TYPE}-${NEW_VER}.${PR_EK}

if [[ ! -e /usr/local/bin/shotcut ]]; then
	echo "Programın güncel versiyonu GİTHUB üzerinden indiriliyor. Bu biraz zaman alabilir, lütfen bekleyin..."
	wget -c -q --show-progress "https://github.com/mltframework/${PR_ADI,,}/releases/download/v${GIT_VER}/${DS_ADI}" -P /tmp/
	
	#Aşağıdaki satırlar alternatif olarak değerlendirilebilir...
	#echo "Programın güncel versiyonu FOSSHUB üzerinden indiriliyor. Bu biraz zaman alabilir, lütfen bekleyin..."
	#wget -c -q --show-progress "https://www.fosshub.com/Shotcut.html\?dwl=${DS_ADI}" -P /tmp/
else
	if [ ${CUR_VER} = ${GIT_VER} ]; then
		echo "Güncel versiyon kullandığınız tespit edildiği için program kapatıldı..."
		exit 0
	elif [ ${CUR_VER//./} -lt ${NEW_VER} ]; then
		echo "Programın güncel versiyonu GİTHUB üzerinden indiriliyor. Bu biraz zaman alabilir, lütfen bekleyin..."
		wget -c -q --show-progress "https://github.com/mltframework/${PR_ADI,,}/releases/download/v${GIT_VER}/${DS_ADI}" -P /tmp/
		
		#Aşağıdaki satırlar alternatif olarak değerlendirilebilir...
		#echo "Programın güncel versiyonu FOSSHUB üzerinden indiriliyor. Bu biraz zaman alabilir, lütfen bekleyin..."
		#wget -c -q --show-progress "https://www.fosshub.com/Shotcut.html\?dwl=${DS_ADI}" -P /tmp/
	else
		echo "Görünüşe göre internetiniz yok..."
		exit 1
	fi
fi

echo "Arşiv dosyası ayıklanıyor..."
tar -xf /tmp/${DS_ADI}

echo -e "\nProgram /opt/ dizini içine yükleniyor/kopyalanıyor... (Şifre isterse girmelisiniz.)"
sudo mv /tmp/${PR_ADI} /opt/${PR_ADI,,}

echo "Programın çalışabilmesi için uygulama PATH dizini içine linkleniyor..."
sudo ln -sfr /opt/${PR_ADI,,}/${PR_ADI}.app/${PR_ADI,,} /usr/local/bin/

echo "Kısayol oluşturuluyor..."
cat > /tmp/${PR_ADI,,}.desktop << EOF
[Desktop Entry]
Name=${PR_ADI}
Comment=Cross-platform Video Editor
GenericName=Video Editor
Exec=/opt/${PR_ADI,,}/${PR_ADI}.app/${PR_ADI,,}
Icon=/opt/${PR_ADI,,}/${PR_ADI}.app/share/icons/hicolor/64x64/apps/org.shotcut.Shotcut.png
Path=/opt/${PR_ADI,,}/${PR_ADI}.app
Type=Application
StartupNotify=true
Terminal=false
Categories=Video;Multimedia;
Keywords=Video;Editor;
EOF

echo "Oluşturulan kısayol uygulamalar menüsüne kopyalanıyor..."
sudo mv /tmp/${PR_ADI,,}.desktop /usr/share/applications/

echo "Program başarılı bir şekilde kuruldu."