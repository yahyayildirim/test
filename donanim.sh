#!/bin/bash

kirmizi=$'\e[1;31m';
sifirla=$'\e[0m';
kalin=$'\e[1m';

if [[ "$(whoami)" != root ]]; then
	exec sudo -- "$0" "$@"
fi

read -p "Lütfen ilçe adını yazın: " ilceniz

zaman=`date +"%m%d%y-%H%M"`
USER_HOME=$(dirname $XAUTHORITY)
kullanici=$(echo $USER_HOME | sed 's|.*/||')
hostname=$(hostname -s)

uretici_adi=$(dmidecode -t system | grep Manufacturer | cut -d ":" -f 2)
uretici_adi=$(echo $uretici_adi | sed -s "s/\ /_/g")
#umodeli=$(dmidecode -t system | grep "Product Name" | cut -d ":" -f 2)
#umodeli=$(echo $umodeli | sed -s "s/\ /_/g")
#echo "Donanım Modeli :"$modeli

#Donanım Üreticisi
uretici=$(dmidecode -t system | grep Manufacturer | cut -d ":" -f 2)
echo "Donanım Üreticisi :$uretici" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Donanım Modeli
modeli=$(dmidecode -t system | grep "Product Name" | cut -d ":" -f 2)
echo "Donanım Modeli :$modeli" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Donanım İşlemcisi
islemci=$(lscpu | grep "Model name:" | cut -d ":" -f 2)
islemcim=$(echo $islemci | sed 's/ //g')
echo "Donanım İşlemcisi : $islemcim" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Çekirdek Sayısı
cekirdek=$(grep -c processor /proc/cpuinfo)
echo "İşlemci Sayısı : $cekirdek Çekirdek" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Donanım RAM
ram_sayisi=$(dmidecode -t memory | grep "Size: [0-9]" | grep -wo [0-9][0-9][0-9][0-9])
toplam_ram=0
for line in ${ram_sayisi[@]}
do
	(( toplam_ram += line ))
done
declare -i total=toplam_ram/1024
echo "Donanım RAM : $total GB" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Donanim Disk
diskler=$(lsblk -n -S --output NAME | grep -v sr.)
disk_sayisi=$(echo $diskler | wc -w)

declare -a disk_turu

x=1
while [ $x -le $disk_sayisi ]
do
	for line in ${diskler[@]};
	do
		dd=$(cat /sys/block/$line/queue/rotational)
		if [ $dd -eq 0 ];then
			disk_turu+=" [Disk-$x $(lsblk -n -S --output TRAN,MODEL,SIZE /dev/$line) SSD]"
		else
			disk_turu+=" [Disk-$x $(lsblk -n -S --output TRAN,MODEL,SIZE /dev/$line) HDD] "
		fi
		x=$(( $x + 1 ))
	done
	echo "Donanım Harddisk :$disk_turu" >> ./${ilceniz}_${kullanici}_${hostname}.txt
done

#Donanım Ana Kart
uretici_main=$(dmidecode -t baseboard | grep Manufacturer | cut -d ":" -f 2)
model_main=$(dmidecode -t baseboard | grep "Product Name" | cut -d ":" -f 2)
echo "Donanım Ana Kartı :$uretici_main$model_main" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Donanım Ekran Kartı
ekran_karti=$(lspci | grep VGA | cut -d ":" -f 3)
echo "Donanım Ekran Kartı :$ekran_karti" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Donanım Ağ Kartı
ethernet_ag_karti=$(lspci | grep "Ethernet controller" | cut -d ":" -f 3)
echo "Donanım Ethernet Ağ Kartı :$ethernet_ag_karti" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Donanım Ses Kartı
ses_karti=$(lspci | grep "Audio device" | cut -d ":" -f 3)
echo "Donanım Ses Kartı :$ses_karti" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#Donanım Kablosuz Ağ kartı
kablosuz_ag_karti=$(lspci | grep "Network controller" | cut -d ":" -f 3)
echo "Donanım Kablosuz Ağ Kartı :$kablosuz_ag_karti" >> ./${ilceniz}_${kullanici}_${hostname}.txt

#dosya isimlerini zaman ekleyerek degisitiriyoruz
cat ./${ilceniz}_${kullanici}_${hostname}.txt | column -t -s ":" -o ":" > ./${ilceniz}_${kullanici}_${hostname}_${zaman}.txt
rm ./${ilceniz}_${kullanici}_${hostname}.txt
chmod 766 ./${ilceniz}_${kullanici}_${hostname}_${zaman}.txt

#ekrana bilgi yazılması
echo "###############################################################"
echo "SAYIN HOCAM;"
echo $PWD/${ilceniz}_${kullanici}_${hostname}_${zaman}.txt
echo "DOSYASI BAŞARILI BİR ŞEKİLDE OLUŞTURULMUŞTUR..."
echo "###############################################################"
