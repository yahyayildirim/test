#!/bin/bash
#################################################
# Yazan		: Yahya YILDIRIM 
# E-Posta	: yahya.yldrm@gmail.com	
# Web		: https://github.com/yahyayildirim/
# Bu script kişisel kullanım için oluşturulmuştur.
#################################################

if [[ "$(whoami)" != root ]]; then
	exec sudo -- "$0" "$@"
fi

#1. Yol
ip=$(ip -o -4 route show to default | awk '{print $5}' | xargs ifconfig -a $1 | grep -w "inet" | awk '{print $2}' | cut -d "." -f 1-3)

#2. Yol
#ip=$(ip a | grep "/24" | cut -d " " -f 8 | cut -d "." -f 1-3)

#3. Yol
#ip=192.168.1

for ek in `seq 1 254`;
do
    sleep 0.05
    ping -c 1 $ip.$ek | grep "64 bytes" | cut -d " " -f 4 | tr -d ":" &
done

