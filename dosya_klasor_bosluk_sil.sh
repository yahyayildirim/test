#!/usr/bin/env bash

# Bu scriptin amacı dosya adlarında boşluk var ise "_" işareti ile değiştirir.
# PDF dosyası olup .pdf uzantısı yazılı olmayanlara .pdf uzantısı ekler. 

# Dikkat edilmesi gereken husus ise PDF dosyalarının scriptin çalıştırdığı klasörde
# veya alt klasörlerde olmasıdır.

klasor_adi_degistir () {
	# Klasör adında boşluk olanları bul ve alttire işareti ile değiştir.
	find $PWD -depth -iname '* *' -type d | while read eski_klasor
	do
		# Boşlukları "_" işareti ile değiştir.
		yeni_klasor=`echo $eski_klasor | tr " " "_"`
		# Eğer aynı dosyadan varsa uyarı yazdır.
		if [ -d "$yeni_klasor" ]; then
			echo -e "$yeni_klasor klasörü zaten var."
			continue
		else
			# Eğer aynı isimli dosya yoksa adını değiştir ve bilgi ver.
			mv "$eski_klasor" "$yeni_klasor" 2>/dev/null
		fi
	done
}


dosya_adi_degistir(){
	# Dosya adında boşluk olanları bul ve alttire işareti ile değiştir.
	find $PWD -depth -iname '* *' -type f | while read eski_dosya
	do
		# Boşlukları "_" işareti ile değiştir.
		yeni_dosya=$(echo $eski_dosya | tr " " "_")
		# Eğer aynı dosyadan varsa uyarı yazdır.
		if [ -f "$yeni_dosya" ]; then
			echo -e "$yeni_dosya dosyası zaten var."
			continue
		else
			# Eğer aynı isimli dosya yoksa adını değiştir ve bilgi ver.
			mv "$eski_dosya" "$yeni_dosya" 2>/dev/null
		fi
	done
}

klasor_adi_degistir
dosya_adi_degistir

#klasor = $(diff -rq ~/Nextcloud/Linux-Pardus/ ~/MEGAsync/ | grep "^Yalnızca" | cut -d" " -f 2 | cut -d"'" -f1)
#dosya = 