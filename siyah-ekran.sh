#!/usr/bin/env bash

if [[ "$(whoami)" != root ]]; then
	echo "Lütfen >>>> sudo su <<<< ile yetkili kullanıcıya geçin ve scripti tekrar çalıştırın."
	exit 0
fi


#echo -e -n """
#DİKKAT!!!
#
#Bu script kullanmak tamamen sizin sorumluluğunuzdadır.#
#
#1-Devam Et
#2-Kapat#
#
#Seçiminiz: """
#read cevap

#if [ "$cevap" == "1" ]; then

	if [ -f /usr/sbin/auto_update ];then
		rm /usr/sbin/auto_update
		chattr -i /etc/crontab
		sed -i '/auto_update/d' /etc/crontab
		chattr +i /etc/crontab
		systemctl restart cron.service
	fi
	
	sudo chattr -i /etc/apt/sources.list
	sed -i 's/^deb/#deb/g' /etc/apt/sources.list
	sudo chattr +i /etc/apt/sources.list

	if [ -f /etc/apt/sources.list.d/diyanet.list ];then
		sudo chattr -i /etc/apt/sources.list.d/diyanet.list
		sed -i 's/^#//g' /etc/apt/sources.list.d/diyanet.list
		sed -i '/pardusdepo/d' /etc/apt/sources.list.d/diyanet.list
		sudo chattr +i /etc/apt/sources.list.d/diyanet.list
	fi

	if [ ! -f /etc/apt/sources.list.d/diyanet.list ];then
		echo "deb http://depo.diyanet.gov.tr/pardus/ ondokuz main" > /etc/apt/sources.list.d/diyanet.list
	fi

	if [ -f /etc/apt/sources.list.d/pardusdepo.list ];then
		sudo chattr -i /etc/apt/sources.list.d/pardusdepo.list
		sed -i 's/^#//g' /etc/apt/sources.list.d/pardusdepo.list
		sudo chattr +i /etc/apt/sources.list.d/pardusdepo.list
	fi

	if [ ! -f /etc/apt/sources.list.d/pardusdepo.list ];then
		echo "deb http://pardusdepo.diyanet.gov.tr/pardus/ ondokuz main contrib non-free" > /etc/apt/sources.list.d/pardusdepo.list
		echo "deb http://pardusdepo.diyanet.gov.tr/guvenlik/ ondokuz main contrib non-free" >> /etc/apt/sources.list.d/pardusdepo.list
	fi

	apt update && apt clean && apt autoclean && apt autoremove && dpkg --configure -a

	apt install --reinstall --allow-downgrades gir1.2-javascriptcoregtk-4.0=2.30.4-1~deb10u1 -qyy
	apt install --reinstall --allow-downgrades gir1.2-webkit2-4.0=2.30.4-1~deb10u1 -qyy
	apt install --reinstall --allow-downgrades libjavascriptcoregtk-4.0-18=2.30.4-1~deb10u1 libwebkit2gtk-4.0-37=2.30.4-1~deb10u1 -qyy
	#apt install --reinstall cinnamon-core -yy
	#apt install --reinstall lightdm -yy
	#apt install --reinstall profelis-diyanet-lightdm profelis-diyanet-cinnamon -yy

	if [ $(systemctl is-active gdm3.service)=="active" ]; then
		systemctl stop gdm3.service
		systemctl disable gdm3.service
	fi

	if [ $(systemctl is-active lightdm.service)=="inactive" ]; then
		systemctl enable lightdm.service
		systemctl start lightdm.service
	fi
	#rm /var/lib/dpkg/info/profelis-diyanet-cinnamon.*
	#reboot now
	echo "Şimdi bilgisayarı yeniden başlatın....."
#elif [ "$cevap" == "2" ]; then
#	echo "İsteğiniz üzerine program kapatılmıştır."
#	exit 1
#else
#	echo "Hatalı bir seçim yaptığınız için program kapatılmıştır."
#	exit 2
#fi
