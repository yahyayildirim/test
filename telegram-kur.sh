#!/bin/bash

set -e
if [[ "$(whoami)" != root ]]; then
	exec sudo -- "$0" "$@"
fi

# Eğer kuruluysa eski sürümü sil
apt purge telegram-desktop 1>/dev/null 2>/dev/null

echo -e "*****  Telegram Son Sürüm İndiriliyor..."
wget -c -q -nc -O /tmp/telegram.tar.xz https://telegram.org/dl/desktop/linux --no-check-certificate --show-progress

echo -e "*****  Arşivlenmiş dosya /opt dizinine çıkartılıyor..."
tar -xvf /tmp/telegram.tar.xz -C /opt/

echo -e "*****  Uygulamalar menüsüne simge ekleniyor..."
echo "[Desktop Entry]
Version=1.0
Name=Telegram Masaüstü
Comment=Telegram Masaüstü Uygulaması
TryExec=/opt/Telegram/Telegram
Exec=/opt/Telegram/Telegram -- %u
Icon=telegram
Terminal=false
StartupWMClass=TelegramDesktop
Type=Application
Categories=Chat;Network;InstantMessaging;Qt;
MimeType=x-scheme-handler/tg;
Keywords=tg;chat;im;messaging;messenger;sms;tdesktop;
X-GNOME-UsesNotifications=true" > /home/$SUDO_USER/.local/share/applications/telegramdesktop.desktop

##Komutların Tamamlandı
echo -e "*****  Kurulum başarılı bir şekilde tamamlandı..."
