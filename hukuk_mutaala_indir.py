#!/usr/bin/env python3

import os
import re
import wget
import requests
from bs4 import BeautifulSoup

"""
KURULUM:
Öncelikle gerekli olan python kütüphaneleri kurulmalı.
sudo python3 -m pip install -U pip
sudo python3 -m pip install -U beautifulsoup4
sudo python3 -m pip install -U wget

KULLANIM:
hukuk_mutaala_indir.py dosyasının olduğu yerde uçbirim açın
python3 hukuk_mutaala_indir.py yazarak enter yapın.

"""

ana_dizin = os.getcwd() # Buradan kasıt mutaala_indir.py dosyanın bulunduğu dizindir.
ev_dizini = ana_dizin + '/Hukuk_Mütalaalar/'

if not os.path.exists(ev_dizini):
	os.makedirs(ev_dizini)

def get_data(url):
	r = requests.get(url)
	soup = BeautifulSoup(r.text, 'html.parser')
	return soup

def parse(soup):
	toplam = 0
	rows = soup.select('tr')
	for row in rows:
		# select only those 'td' tags with class 'detail'
		detailCells = row.select('a')
		for a in detailCells:
			link = 'https://hukukmusavirligi.diyanet.gov.tr' + a.attrs['href']
			baslik = a.text.strip().replace(" ", "_")
			title = re.sub("%20|/|\'|,|\(|\)|-|$|\"|\.|’|“|”", "_", baslik)
			if len(title) < 5:
				continue
			else:
				yol = ev_dizini + f"{title[0:150]}" + '.pdf'
				if os.path.exists(yol) == True:
					continue
				else:
					link = link.replace("%20", " ")
					wget.download(link, out=yol)
					toplam = toplam + 1

if __name__ == '__main__':
	url = 'https://hukukmusavirligi.diyanet.gov.tr/kategoriler/hizmetlerimiz'
	soup = get_data(url)
	parse(soup)
