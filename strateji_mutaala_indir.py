#!/usr/bin/env python3

import os
import wget
import requests
from bs4 import BeautifulSoup

"""
KURULUM:
Öncelikle gerekli olan python kütüphaneleri kurulmalı.
sudo python3 -m pip install -U pip
sudo python3 -m pip install -U beautifulsoup4
sudo python3 -m pip install -U wget

KULLANIM:
strateji_mutaala_indir.py dosyasının olduğu yerde uçbirim açın
python3 strateji_mutaala_indir.py yazarak enter yapın.

"""

url = 'https://stratejigelistirme.diyanet.gov.tr/sayfa/25'
ana_dizin = os.getcwd() # Buradan kasıt mutaala_indir.py dosyanın bulunduğu dizindir.
indirme_dizini = ana_dizin + '/Strateji_Mütalaalar/'

if not os.path.exists(indirme_dizini):
        os.makedirs(indirme_dizini, exist_ok=True)

def get_data(url):
	r = requests.get(url)
	soup = BeautifulSoup(r.text, 'html.parser')
	return soup

def parse(soup):
	toplam = 0
	sonuclar = soup.find_all('div', {'class': 'document-link-item'})
	for item in sonuclar:
		product = {
				'title': item.find('a', {'class': 'document-link'}).text.strip(),
				'link': item.find('a', {'class': 'document-link'})['href'],
		}
		link = 'https://stratejigelistirme.diyanet.gov.tr' + product['link']
		title = product['title'].replace("/", "_")
		yol = indirme_dizini + title + '.pdf'
		if os.path.exists(yol) == True:
			continue
		else:
			wget.download(link, out=yol)
			toplam = toplam + 1
	print("\nToplam " + str(toplam) + " adet pdf dosyası indirildi.")

if __name__ == '__main__':
	soup = get_data(url)
	parse(soup)
